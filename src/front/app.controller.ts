import { Controller, Get, Logger, Redirect } from '@nestjs/common';

@Controller()
export class AppController {
  private readonly logger = new Logger(AppController.name);

  @Get()
  @Redirect('/auth/login')
  home() {
    this.logger.log('go to (home page / login)');
  }
}
