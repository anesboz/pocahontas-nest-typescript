import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  ParseIntPipe,
  Post,
  Render,
  Response,
  UseFilters,
  UseGuards,
} from '@nestjs/common';
import { AuthenticatedGuard } from '../../api/auth/guard/authenticated.guard';
import { ViewAuthFilter } from '../../api/auth/filter/view.auth.filter';
import { GuaranteeService } from '../../api/guarantee/application/guarantee.service';
import { GuaranteeCommand } from 'src/api/guarantee/application/guarantee.command';
import { GuaranteeCategory } from 'src/api/guarantee/domain/categories/categories.enum';

@Controller('guarantee')
@UseFilters(ViewAuthFilter)
@UseGuards(AuthenticatedGuard)
export class GuaranteeController {
  @Inject(GuaranteeService)
  private readonly service: GuaranteeService;

  @Get()
  @Render('guarantee/index')
  async index() {
    const items = await this.service.find();
    return { items };
  }

  @Get(`create`)
  @Render('guarantee/create')
  async createRender() {
    return { categories: Object.values(GuaranteeCategory) };
  }
  @Post(`create`)
  create(@Body() command: GuaranteeCommand, @Response() res) {
    this.service.create(command);
    res.redirect('/guarantee');
    return;
  }

  @Get('delete/:id')
  async delete(@Param('id', ParseIntPipe) id: number, @Response() res) {
    await this.service.delete(id);
    res.redirect('../');
  }
}
