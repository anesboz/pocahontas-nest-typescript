import { Quote } from '../../../api/quote/domain/quote.entity';
import { LogementType } from '../../../api/quote/domain/logement/logement.type';
import { ResidenceType } from '../../../api/quote/domain/logement/residence.type';
import { OccupantType } from '../../../api/quote/domain/occupant/occupant.type';

export const quoteVm = (quote: Quote) => {
  const { logement } = quote;
  const { occupant } = quote;
  const { address } = logement;

  return {
    id: quote.id,
    logementType: logementTypeVm(logement.logementType),
    residenceType: residenceTypeVm(logement.residenceType),
    occupantType: occupantTypeVm(occupant.type),
    isStudent: isStudentVm(occupant.isStudent),
    isLivingOnResidence: isLivingOnResidenceVm(occupant.isLivingOnResidence),
    address: address.toString(),
    updated: quote.updatedAt,
  };
};
export const occupantTypeVm = (type: OccupantType): string =>
  type === OccupantType.LOCATAIRE
    ? 'Locataire'
    : OccupantType.PROPRIETAIRE
    ? 'Proprietaire'
    : 'Colocataire';

export const logementTypeVm = (type: LogementType): string =>
  type === LogementType.APARTMENT ? 'Appartement' : 'Maison';

export const isStudentVm = (type: boolean): boolean => type === true;
export const isLivingOnResidenceVm = (type: boolean): boolean => type === true;

export const residenceTypeVm = (type: ResidenceType): string =>
  `Résidence ${type === ResidenceType.MAIN ? 'Principale' : 'Secondaire'}`;

export const LOGEMENT_TYPES = [
  {
    value: 'home',
    name: 'logementType',
    label: 'Une maison',
    icon: 'https://devis-habitation.direct-assurance.fr/assets/images/questionnaire_icons/house.svg',
  },
  {
    value: 'apartment',
    name: 'logementType',
    label: 'Un appartement',
    icon: 'https://devis-habitation.direct-assurance.fr/assets/images/questionnaire_icons/appartement.svg',
  },
];

export const RESIDENCE_TYPES = [
  {
    value: 'main',
    name: 'residenceType',
    label: 'Ma résidence principale',
    icon: 'https://devis-habitation.direct-assurance.fr/assets/images/questionnaire_icons/house.svg',
  },
  {
    value: 'second',
    name: 'residenceType',
    label: 'Ma résidence secondaire',
    icon: 'https://devis-habitation.direct-assurance.fr/assets/images/questionnaire_icons/residence_secondaire_inactif.svg',
  },
];

export const OCCUPANT_TYPES = [
  {
    value: 'locataire',
    name: 'type',
    label: 'Locataire',
    icon: 'https://devis-habitation.direct-assurance.fr/assets/images/questionnaire_icons/locataire_inactif.svg',
  },
  {
    value: 'colocataire',
    name: 'type',
    label: 'Colocataire',
    icon: 'https://devis-habitation.direct-assurance.fr/assets/images/questionnaire_icons/colocataire_inactif.svg',
  },
  {
    value: 'proprietaire',
    name: 'type',
    label: 'Propriétaire',
    icon: 'https://devis-habitation.direct-assurance.fr/assets/images/questionnaire_icons/proprietaire_inactif.svg',
  },
];

export const IS_STUDENT = [
  {
    value: 'true',
    name: 'isStudent',
    label: 'Étudiant',
  },
  {
    value: 'false',
    name: 'isStudent',
    label: 'Autre',
  },
];

export const IS_LIVING_ON_RESIDENCE = [
  {
    value: 'true',
    name: 'isLivingOnResidence',
    label: 'Un Logement Actuel',
    icon: 'https://devis-habitation.direct-assurance.fr/assets/images/questionnaire_icons/house.svg',
  },
  {
    value: 'false',
    name: 'isLivingOnResidence',
    label: 'Un Logement Futur',
    icon: 'https://devis-habitation.direct-assurance.fr/assets/images/questionnaire_icons/residence_secondaire_inactif.svg',
  },
];
