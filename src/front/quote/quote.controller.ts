import {
  Controller,
  Get,
  Inject,
  Param,
  ParseIntPipe,
  Render,
  Request,
  Response,
  UseFilters,
  UseGuards,
} from '@nestjs/common';
import { QuoteUsecase } from '../../api/quote/application/quote.usecase';
import { AuthenticatedGuard } from '../../api/auth/guard/authenticated.guard';
import { ViewAuthFilter } from '../../api/auth/filter/view.auth.filter';
import { quoteVm } from './vm/quote.vm';
import { Role } from '../../api/users/domain/role/role.enum';

@Controller('front')
@UseFilters(ViewAuthFilter)
@UseGuards(AuthenticatedGuard)
export class QuoteController {
  @Inject(QuoteUsecase)
  private readonly quoteUsecase: QuoteUsecase;

  @Get()
  @Render('index')
  index(@Request() req) {
    return { isAdmin: req.user?.roles?.includes(Role.Admin) };
  }

  // @Post('subscribe')
  // @Render('quote/quote')
  // async subscribe(@Body() data: CreateQuoteCommand) {
  //   return this.quoteUsecase.create(data);
  // }

  @Get('list')
  @Render('quote/list')
  async list() {
    const quotes = await this.quoteUsecase.findAll();
    return { quotes: quotes.map(quoteVm) };
  }

  @Get('quote/delete/:quoteId')
  async delete(
    @Param('quoteId', ParseIntPipe) quoteId: number,
    @Response() res,
  ) {
    await this.quoteUsecase.deleteById(quoteId);
    res.redirect('../../list');
  }
}
