import {
  Body,
  Controller,
  Get,
  Post,
  Render,
  Req,
  Response,
  Request,
} from '@nestjs/common';
import { CreateQuoteCommand } from '../../../api/quote/application/quote.command';
import { QuoteUsecase } from '../../../api/quote/application/quote.usecase';
import {
  LOGEMENT_TYPES,
  OCCUPANT_TYPES,
  RESIDENCE_TYPES,
  IS_STUDENT,
  IS_LIVING_ON_RESIDENCE,
} from '../vm/quote.vm';

@Controller('front/quote/new')
export class CreateQuoteController {
  constructor(private readonly usecase: QuoteUsecase) {}

  @Get()
  @Render('quote/index')
  index() {
    return {
      logementTypes: LOGEMENT_TYPES,
      residenceTypes: RESIDENCE_TYPES,
      occupantTypes: OCCUPANT_TYPES,
      isStudent: IS_STUDENT,
      isLivingOnResidence: IS_LIVING_ON_RESIDENCE,
    };
  }

  @Post()
  async create(
    @Request() req,
    @Response() res,
    @Body() command: CreateQuoteCommand,
  ) {
    const quote = await this.usecase.create(command, req.user.id);
    res.redirect(`/offer/quote/${quote.id}`);
    return;
  }
}
