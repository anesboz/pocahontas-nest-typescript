import { Test, TestingModule } from '@nestjs/testing';
import { CreateQuoteController } from './create-quote.controller';
import { QuoteUsecase } from '../../../api/quote/application/quote.usecase';

describe('CreateQuoteController', () => {
  let controller: CreateQuoteController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CreateQuoteController],
      providers: [
        QuoteUsecase,
        {
          provide: QuoteUsecase,
          useValue: {
            get: jest.fn(),
            findAll: jest.fn(),
            create: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<CreateQuoteController>(CreateQuoteController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
