import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  ParseIntPipe,
  Post,
  Render,
  Request,
  Response,
  UseFilters,
  UseGuards,
} from '@nestjs/common';
import { AuthenticatedGuard } from '../../api/auth/guard/authenticated.guard';
import { ViewAuthFilter } from '../../api/auth/filter/view.auth.filter';
import { OfferService } from '../../api/offer/application/offer.service';
import { GuaranteeService } from 'src/api/guarantee/application/guarantee.service';
import { GuaranteeCategory } from 'src/api/guarantee/domain/categories/categories.enum';
import { Guarantee } from 'src/api/guarantee/domain/guarantee.entity';
import { CreateOfferCommand } from 'src/api/offer/application/offer.command';
import { QuoteUsecase } from 'src/api/quote/application/quote.usecase';
import { Offer } from 'src/api/offer/domain/offer.entity';
import { Contract } from 'src/api/contracts/domain/contract.entity';
import { ContractsService } from 'src/api/contracts/application/contracts.service';

@Controller('offer')
@UseFilters(ViewAuthFilter)
@UseGuards(AuthenticatedGuard)
export class OfferController {
  @Inject(OfferService)
  private readonly service: OfferService;
  @Inject(GuaranteeService)
  private readonly guaranteeService: GuaranteeService;
  @Inject(QuoteUsecase)
  private readonly quoteService: QuoteUsecase;
  @Inject(ContractsService)
  private readonly contractsService: ContractsService;

  @Get()
  @Render('offer/index')
  async index() {
    const items = await this.service.findAll();
    return { items };
  }

  @Get('quote/:id')
  @Render('offer/propositions')
  async suggest(@Param('id', ParseIntPipe) quoteId: number) {
    const quote = await this.quoteService.get(quoteId);
    const offers = await this.service.findAll();
    const prices = [];
    offers.forEach((offer) => {
      prices[offer.id] =
        this.quoteService.calcPrime(quote) + offer.guarantees.length;
    });

    return {
      offers,
      quoteId,
      prices,
    };
  }

  @Get(`create`)
  @Render('offer/create')
  async createRender() {
    const guarantees = await this.guaranteeService.find();
    return { guarantees };
  }

  @Post(`create`)
  create(@Body() command: CreateOfferCommand, @Response() res) {
    this.service.create(command);
    res.redirect('/offer');
  }

  @Get('delete/:id')
  async delete(@Param('id', ParseIntPipe) id: number, @Response() res) {
    await this.service.delete(id);
    res.redirect('../');
  }

  @Post(`choose`)
  async choose(@Request() req, @Body() body: any, @Response() res) {
    const { quoteId, offerId, price } = body;

    const quote = await this.quoteService.get(quoteId);
    const offer = await this.service.get(offerId);

    quote.prime = this.quoteService.calcPrime(quote) + offer.guarantees.length;

    await this.quoteService.update(quote);

    const contract = await this.contractsService.create(req.user.id, {
      quote,
      offer,
      auto_renewal: false,
    });

    res.redirect(`/payment/${contract.id}`);

    // res.redirect(`/contracts/${contract.id}`);
  }
}
