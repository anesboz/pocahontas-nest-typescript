import {
  Body,
  Controller,
  Get,
  Post,
  Render,
  Request,
  Response,
  UseGuards,
} from '@nestjs/common';
import { LocalAuthGuard } from '../../api/auth/guard/local-auth.guard';
import { AuthService } from '../../api/auth/application/auth.service';
import { UserCommand } from '../../api/users/application/user.command';
import { adminInfo } from '../../api/users/domain/admin.info';

@Controller(`auth`)
export class AuthController {
  constructor(private readonly service: AuthService) {}

  @Get('login')
  @Render('auth/login')
  loginPage() {
    return { message: 'Login' };
  }

  // s'autentifier
  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req, @Response() res) {
    res.redirect('/front');
  }

  // logout
  @Get('logout')
  async logout(@Request() req, @Response() res) {
    req.session.destroy(null);
    // res.clearCookie(`connect.sid`, { path: '/' });
    // req.logout();
    // res.redirect('/');
  }

  @Get('signup')
  @Render('auth/signup')
  async signupPage() {
    return { message: 'Create new account' };
  }

  @Post('signup')
  async signup(@Body() command: UserCommand, @Response() res) {
    await this.service.signup(command);
    res.redirect('/auth/login');
  }

  @Post('admin')
  async createAdmin() {
    await this.service.signup(adminInfo);
  }
}
