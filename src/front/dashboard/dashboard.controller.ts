import {
  Controller,
  Get,
  Render,
  Request,
  UseFilters,
  UseGuards,
} from '@nestjs/common';
import { ViewAuthFilter } from 'src/api/auth/filter/view.auth.filter';
import { AuthenticatedGuard } from 'src/api/auth/guard/authenticated.guard';
import { ContractsService } from '../../api/contracts/application/contracts.service';

@Controller('front/dashboard')
@UseFilters(ViewAuthFilter)
@UseGuards(AuthenticatedGuard)
export class DashboardController {
  constructor(private readonly contractsService: ContractsService) {}

  @Get()
  @Render('dashboard/index')
  async dashboard(@Request() req) {
    const contracts = await this.contractsService.findContractsByUser(
      req.user.id,
    );

    const all: number[] = contracts.map(({ id }) => id);
    const valid: number[] = [];
    const toSigne: number[] = [];
    const cancelled: number[] = [];

    // un tour de boucle
    contracts.map(({ id, signed_at, cancelled_at }) => {
      if (signed_at == null) {
        toSigne.push(id);
        return;
      }

      if (cancelled_at == null) {
        valid.push(id);
        return;
      }

      cancelled.push(id);
    });

    return { contracts: { all, valid, toSigne, cancelled } };
  }
}
