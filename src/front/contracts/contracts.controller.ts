import {
  Controller,
  Get,
  Inject,
  Post,
  Render,
  UseFilters,
  UseGuards,
  Request,
  Param,
  Response,
  Query,
} from '@nestjs/common';
import { ContractsService } from '../../api/contracts/application/contracts.service';
import { AuthenticatedGuard } from '../../api/auth/guard/authenticated.guard';
import { ViewAuthFilter } from '../../api/auth/filter/view.auth.filter';
import { MailerService } from '@nestjs-modules/mailer';
import { PaymentsService } from '../../api/payments/application/payments.service';
import { Contract } from 'src/api/contracts/domain/contract.entity';

@Controller('contracts')
@UseFilters(ViewAuthFilter)
@UseGuards(AuthenticatedGuard)
export class ContractsController {
  constructor(private mailService: MailerService) {}
  @Inject(ContractsService)
  private readonly contractsService: ContractsService;

  @Get('list')
  @Render('contracts/list')
  async list(@Request() req) {
    const contracts = await this.contractsService.findContractsByUser(
      req.user.id,
    );
    return { contracts };
  }

  @Get('some')
  @Render('contracts/list')
  async listPartial(@Request() req, @Query() params) {
    const ids: number[] = params.ids
      .split(',')
      // case empty array
      .filter((e) => e)
      .map((e: string) => parseInt(e));
    const contracts: Contract[] = [];
    for (const id of ids) {
      const contract = await this.contractsService.get(id);
      contracts.push(contract);
    }
    return { contracts };
  }

  @Post('cancel/:id')
  async cancel(@Param() params, @Response() res) {
    await this.contractsService.cancel(params.id);
    res.redirect(`/contracts/${params.id}`);
  }

  @Post('sign/:id')
  async sign(@Param() params, @Response() res) {
    const contract = await this.contractsService.get(params.id);
    await this.contractsService.sign(params.id);
    await this.sendmail(params.id, false);
    res.redirect(`/contracts/${params.id}`);
  }

  @Post('renewal/:id')
  async renewal(@Param() params, @Response() res) {
    await this.contractsService.renewal(params.id);
    await this.sendmail(params.id, true);
    res.redirect(`/contracts/${params.id}`);
  }

  @Get(':id')
  @Render('contracts/detail')
  async contractDetails(@Param() params) {
    const contract = await this.contractsService.get(params.id);
    const refundable = await this.contractsService.isContractRefundable(
      params.id,
    );
    return { contract, refundable };
  }

  private async sendmail(contractID: number, renewed: boolean) {
    const contract = await this.contractsService.get(contractID);
    const email = contract.client.user.email;
    const suffix = renewed ? ` renewed` : `singed`;
    const text = `Bienvenue chez poca hontas assurance,\nYour contract has been ${suffix}.`;
    const response = await this.mailService.sendMail({
      from: 'djedjiaissat@gmail.com',
      to: email,
      subject: 'Souscription à un contrat chez PocaHontas',
      text,
    });
    return response;
  }
}
