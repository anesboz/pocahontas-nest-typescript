import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  ParseIntPipe,
  Post,
  Render,
  Request,
  Response,
  UseFilters,
  UseGuards,
} from '@nestjs/common';
import { AuthenticatedGuard } from '../../api/auth/guard/authenticated.guard';
import { ViewAuthFilter } from '../../api/auth/filter/view.auth.filter';
import { ContractsService } from 'src/api/contracts/application/contracts.service';
import { PaymentsService } from 'src/api/payments/application/payments.service';

@Controller('payment')
@UseFilters(ViewAuthFilter)
@UseGuards(AuthenticatedGuard)
export class PaymentController {
  @Inject(ContractsService)
  private readonly contractsService: ContractsService;
  @Inject(PaymentsService)
  private readonly paymentsService: PaymentsService;

  @Get(':contractId')
  @Render('payment/payment_page')
  async paymentPage(@Param('contractId', ParseIntPipe) contractId: number) {
    const contract = await this.contractsService.getFullContract(contractId);
    return { contractId, price: contract.quote.prime };
  }

  @Post('pay/:contractId')
  async pay(@Body() body, @Param() params, @Response() res) {
    // const contract = await this.contractsService.get(params.contractId);

    const payment = await this.paymentsService.create(params.contractId, {
      price: body.price,
    });

    res.redirect(`/contracts/${params.contractId}`);
  }
}
