import {
  Body,
  Controller,
  Get,
  Post,
  Render,
  Request,
  Response,
  UseFilters,
  UseGuards,
} from '@nestjs/common';
import { AuthenticatedGuard } from '../../api/auth/guard/authenticated.guard';
import { ViewAuthFilter } from '../../api/auth/filter/view.auth.filter';
import { Role } from '../../api/users/domain/role/role.enum';
import { UsersService } from 'src/api/users/application/users.service';
import { UninsurableZoneService } from 'src/api/quote/application/uninsurable-zone.service';
import { Roles } from 'src/api/users/domain/role/roles.decorator';
import { User } from 'src/api/users/domain/user.entity';
import { UninsurableZoneCommand } from 'src/api/quote/application/UninsurableZoneCommand';

@Controller('admin')
@UseFilters(ViewAuthFilter)
@UseGuards(AuthenticatedGuard)
@Roles(Role.Admin)
export class AdminController {
  constructor(
    private readonly service: UsersService,
    private readonly zoneService: UninsurableZoneService,
  ) {}

  @Get()
  @Render('admin/index')
  index(@Request() req) {
    return { isAdmin: req.user?.roles?.includes(Role.Admin) };
  }

  async getUsers(): Promise<User[]> {
    return this.service.find();
  }

  @Get('uninsured-zone')
  @Render(`admin/uninsured-zone`)
  async getUninsuredZones() {
    const zones = await this.zoneService.find();
    console.info(zones);
    return { zones };
  }

  @Get('uninsured-zone/add')
  @Render(`admin/add-uninsured-zone`)
  async addUninsuredZonesForm() {
    return { message: 'Ajouter une zone' };
  }

  @Post('uninsured-zone/add')
  async addUninsuredZones(
    @Response() res,
    @Body() zone: UninsurableZoneCommand,
  ) {
    await this.zoneService.add(zone);
    res.redirect('/admin/uninsured-zone');
  }
}
