import { INestApplication } from '@nestjs/common';
import * as session from 'express-session';
import * as passport from 'passport';

const SESSION_MAX_AGE = 3600000;
const SESSION_SECRET = 'changeme';

export function setupSession(app: INestApplication) {
  app.use(
    session({
      secret: SESSION_SECRET,
      resave: false,
      saveUninitialized: false,
      cookie: { maxAge: SESSION_MAX_AGE },
    }),
  );
  app.use(passport.initialize());
  app.use(passport.session());
}
