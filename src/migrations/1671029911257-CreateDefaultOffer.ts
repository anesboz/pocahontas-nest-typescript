import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateDefaultOffer1671029911257 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `INSERT INTO offer (id, name) VALUES ((SELECT nextval('offer_id_seq')), 'default')`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DELETE FROM offer WHERE name = 'default'`);
  }
}
