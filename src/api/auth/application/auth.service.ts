import { Injectable } from '@nestjs/common';
import { UsersService } from '../../users/application/users.service';
import { UserCommand } from '../../users/application/user.command';
import { User } from '../../users/domain/user.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(private usersService: UsersService) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOneByUsername(username);
    const isMatch = user && (await bcrypt.compare(pass, user.password));
    if (isMatch) {
      const { ...result } = user;
      return result;
    }
    return null;
  }

  async signup(command: UserCommand): Promise<User> {
    const salt = await bcrypt.genSalt();
    const password = await bcrypt.hash(command.password, salt);
    return this.usersService.save({ ...command, password });
  }
}
