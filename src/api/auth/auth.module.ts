import { Module } from '@nestjs/common';
import { AuthService } from './application/auth.service';
import { UsersModule } from '../users/users.module';
import { LocalStrategy } from './strategy/local.strategy';
import { AuthenticatedGuard } from './guard/authenticated.guard';
import { PassportModule } from '@nestjs/passport';
import { SessionSerializer } from './config/session.serializer';
import { ViewAuthFilter } from './filter/view.auth.filter';
import { AlreadyAuthMiddleware } from './guard/already-auth.middleware';

@Module({
  imports: [UsersModule, PassportModule.register({ session: true })],
  providers: [
    AuthService,
    LocalStrategy,
    AuthenticatedGuard,
    SessionSerializer,
    ViewAuthFilter,
    AlreadyAuthMiddleware,
  ],
  exports: [
    AuthenticatedGuard,
    ViewAuthFilter,
    AuthService,
    AlreadyAuthMiddleware,
  ],
})
export class AuthModule {}
