import {
  createParamDecorator,
  Injectable,
  NestMiddleware,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

@Injectable()
export class AlreadyAuthMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    if (req.isAuthenticated()) {
      res.redirect('/front');
    }
    next();
  }
}

export const User = createParamDecorator((data, req) => {
  return req.user;
});
