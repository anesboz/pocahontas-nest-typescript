import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Contract } from '../../contracts/domain/contract.entity';
import { Repository } from 'typeorm';
import { Client } from '../../clients/domain/client.entity';
import { Payment } from '../domain/payments.entity';

@Injectable()
export class PaymentsService {
  constructor(
    @InjectRepository(Contract)
    private readonly contractRepository: Repository<Contract>,
    @InjectRepository(Payment)
    private readonly paymentRepository: Repository<Payment>,
  ) {}
  public async create(contractId: number, data: any): Promise<Payment> {
    const contract = await this.contractRepository.findOne({
      where: { id: contractId },
    });
    const payment: Payment = new Payment({
      ...data,
      contract,
    });

    return await this.paymentRepository.save(payment);
  }
}
