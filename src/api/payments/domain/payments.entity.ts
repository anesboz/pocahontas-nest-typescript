import { Client } from '../../clients/domain/client.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Contract } from '../../contracts/domain/contract.entity';

@Entity()
export class Payment {
  @PrimaryGeneratedColumn()
  id!: number;

  @ManyToOne(() => Contract, (contract) => contract.payments)
  @JoinColumn()
  contract: Contract;

  @Column({ nullable: true })
  price!: number;

  @Column({ nullable: true, default: false })
  refund!: boolean;

  @Column({ type: 'timestamp', nullable: true })
  public executedAt!: Date;

  @Column({ type: 'timestamp', nullable: true })
  public canceledAt!: Date;

  /*
   * Create and Update Date Columns
   */

  @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  public createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  public updatedAt!: Date;

  constructor(partial: Partial<Payment>) {
    Object.assign(this, partial);
  }
}
