import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PaymentsService } from './application/payments.service';
import { Client } from '../clients/domain/client.entity';
import { User } from '../users/domain/user.entity';
import { Contract } from '../contracts/domain/contract.entity';
import { Payment } from './domain/payments.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Contract, User, Client, Payment])],
  providers: [PaymentsService],
  exports: [PaymentsService],
})
export class PaymentsModule {}
