import { Contract } from '../../contracts/domain/contract.entity';
import { Guarantee } from '../../guarantee/domain/guarantee.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  OneToMany,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Offer {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column()
  name: string;

  @ManyToMany(() => Guarantee)
  @JoinTable()
  guarantees: Guarantee[];

  @OneToMany(() => Contract, (contract) => contract.offer)
  contracts: Contract[];

  /*
   * Create and Update Date Columns
   */
  @CreateDateColumn({ type: 'timestamp' })
  public createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  public updatedAt!: Date;

  constructor(props: Partial<Offer>) {
    Object.assign(this, props);
  }
}
