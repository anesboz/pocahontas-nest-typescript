import { CreateOfferCommand } from './offer.command';
import { Offer } from '../domain/offer.entity';

export const toModel = (command: CreateOfferCommand): Offer => {
  return new Offer({});
};
