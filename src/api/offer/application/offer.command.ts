import { IsNumber, IsString } from 'class-validator';

export class CreateOfferCommand {
  @IsString()
  name: string;

  @IsString({ each: true })
  guaranteesIds: string[];
}
