import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Guarantee } from '../../guarantee/domain/guarantee.entity';
import { In, Repository } from 'typeorm';
import { Offer } from '../domain/offer.entity';
import { CreateOfferCommand } from './offer.command';

@Injectable()
export class OfferService {
  static count = 0;
  constructor(
    @InjectRepository(Offer)
    private readonly repository: Repository<Offer>,
    @InjectRepository(Guarantee)
    private readonly guaranteeRepository: Repository<Guarantee>,
  ) {}

  public async create(command: CreateOfferCommand): Promise<Offer> {
    const { name } = command;
    const guaranteesIds = command.guaranteesIds.map((e) => parseInt(e));

    const guarantees = await this.guaranteeRepository.findBy({
      id: In(guaranteesIds),
    });

    const offer: Offer = new Offer({
      name,
      guarantees,
    });
    return await this.repository.save(offer);
  }

  findAll(): Promise<Offer[]> {
    return this.repository.find({
      relations: {
        guarantees: true,
      },
    });
  }

  public async get(id: number): Promise<Offer> {
    return await this.repository.findOne({
      where: { id },
      relations: { guarantees: true },
    });
  }

  delete(id: number) {
    return this.repository.delete({ id });
  }
}
