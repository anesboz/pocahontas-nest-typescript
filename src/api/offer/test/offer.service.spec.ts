import { Test, TestingModule } from '@nestjs/testing';
import { Repository } from 'typeorm';
import { createMock } from '@golevelup/ts-jest';
import { getRepositoryToken } from '@nestjs/typeorm';
import { GuaranteeService } from '../../guarantee/application/guarantee.service';
import { OfferService } from '../application/offer.service';
import { Offer } from '../domain/offer.entity';
import { Guarantee } from '../../guarantee/domain/guarantee.entity';

describe('Offer Service', () => {
  let underTest: OfferService;
  let repo: Repository<Offer>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        OfferService,
        {
          provide: getRepositoryToken(Offer),
          useValue: createMock<Repository<Offer>>(),
        },
        GuaranteeService,
        {
          provide: getRepositoryToken(Guarantee),
          useValue: createMock<Repository<Guarantee>>(),
        },
      ],
    }).compile();

    underTest = module.get<OfferService>(OfferService);
    repo = module.get<Repository<Offer>>(getRepositoryToken(Offer));
  });

  it('should be defined', () => {
    expect(underTest).toBeDefined();
  });

  it('Delete offer', async () => {
    // given
    repo.delete = jest.fn();

    // when
    await underTest.delete(1);

    // then
    expect(repo.delete).toBeCalledTimes(1);
    expect(repo.delete).toBeCalledWith({ id: 1 });
  });

  it('FindAll offers', async () => {
    // given
    const expected = [
      new Offer({ id: 1 }),
      new Offer({ id: 2 }),
      new Offer({ id: 3 }),
    ];
    repo.find = jest.fn().mockResolvedValue(expected);

    // when
    const quotes = await underTest.findAll();

    // then
    expect(quotes).toEqual(expected);
    expect(repo.find).toBeCalledTimes(1);
  });
});
