import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Offer } from './domain/offer.entity';
import { OfferService } from './application/offer.service';
import { Guarantee } from '../guarantee/domain/guarantee.entity';
import { OfferController } from 'src/front/offer/offer.controller';
import { GuaranteeService } from '../guarantee/application/guarantee.service';
import { QuoteUsecase } from 'src/api/quote/application/quote.usecase';
import { Quote } from '../quote/domain/quote.entity';
import { Client } from '../clients/domain/client.entity';
import { Contract } from '../contracts/domain/contract.entity';
import { ContractsService } from '../contracts/application/contracts.service';
import { AdresseProxy } from '../quote/infra/adresse.api/adresse.api.service';
import { HttpModule } from '@nestjs/axios';
import { UninsurableZoneEntity } from '../quote/domain/uninsurable-zone/uninsurable-zone.entity';
import { Payment } from '../payments/domain/payments.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Offer,
      Guarantee,
      Quote,
      Client,
      Contract,
      UninsurableZoneEntity,
      Payment,
    ]),
    HttpModule,
  ],
  controllers: [OfferController],
  providers: [
    OfferService,
    GuaranteeService,
    QuoteUsecase,
    ContractsService,
    AdresseProxy,
  ],
  exports: [OfferService, GuaranteeService],
})
export class OfferModule {}
