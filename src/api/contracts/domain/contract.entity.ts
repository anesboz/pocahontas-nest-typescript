import { Client } from '../../clients/domain/client.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Quote } from '../../quote/domain/quote.entity';
import { Offer } from '../../offer/domain/offer.entity';
import { Payment } from '../../payments/domain/payments.entity';

@Entity()
export class Contract {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ type: 'bigint', nullable: true })
  num: number;

  @Column({ type: 'varchar', unique: true, length: 120, nullable: true })
  uuid: string;

  @ManyToOne(() => Client, (client) => client.contracts)
  @JoinColumn()
  client: Client;

  @OneToMany(() => Payment, (payment) => payment.contract)
  payments: Payment[];

  @Column({ type: 'timestamptz' })
  start_date: Date;

  @Column({ type: 'timestamptz' })
  end_date: Date;

  @Column()
  auto_renewal: boolean;

  @Column({ type: 'timestamptz', nullable: true })
  signed_at!: Date;

  @Column({ type: 'timestamptz', nullable: true })
  cancelled_at!: Date;

  @OneToOne(() => Quote)
  @JoinColumn()
  quote: Quote;

  /*
   * Create and Update Date Columns
   */

  @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  public createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  public updatedAt!: Date;

  // TODO: Add offer relationship once entity is available
  @ManyToOne(() => Offer)
  @JoinColumn()
  offer: Offer;

  constructor(partial: Partial<Contract>) {
    Object.assign(this, partial);
  }
}
