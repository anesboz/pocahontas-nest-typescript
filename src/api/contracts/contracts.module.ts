import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Contract } from './domain/contract.entity';
import { ContractsService } from './application/contracts.service';
import { Client } from '../clients/domain/client.entity';
import { User } from '../users/domain/user.entity';
import { Payment } from '../payments/domain/payments.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Contract, User, Client, Payment])],
  providers: [ContractsService],
  exports: [ContractsService],
})
export class ContractsModule {}
