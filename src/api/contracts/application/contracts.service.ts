import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Contract } from '../domain/contract.entity';
import { Repository } from 'typeorm';
import { Client } from '../../clients/domain/client.entity';
import { Payment } from '../../payments/domain/payments.entity';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class ContractsService {
  constructor(
    @InjectRepository(Contract)
    private readonly repository: Repository<Contract>,
    @InjectRepository(Client)
    private readonly clientRepository: Repository<Client>,
    @InjectRepository(Payment)
    private readonly paymentRepository: Repository<Payment>,
  ) {}

  public async findContractsByUser(userId: number): Promise<Contract[]> {
    return await this.repository.find({
      where: {
        client: { user: { id: userId } },
      },
      relations: {
        client: { user: true },
      },
    });
  }

  public async get(id: number): Promise<Contract> {
    return await this.repository.findOne({
      where: { id },
      relations: { client: { user: true }, quote: true },
    });
  }

  public async getFullContract(id: number): Promise<Contract> {
    return await this.repository.findOne({
      where: {
        id,
      },
      relations: {
        quote: true,
      },
    });
  }

  public async isContractRefundable(id: number): Promise<boolean> {
    const contract = await this.repository.findOneBy({ id });
    const today_date = new Date();
    const signed_date = contract.signed_at;
    return today_date <= this.addDaysToDate(signed_date, 14);
  }

  public async cancel(id: number): Promise<Contract> {
    const contract = await this.repository.findOneBy({ id });
    // Check refund policy
    if (await this.isContractRefundable(id)) {
      const payment = await this.paymentRepository.findOne({
        where: { contract: { id }, refund: false },
      });
      if (payment?.executedAt) this.refundPayment(contract, payment);
      else
        await this.paymentRepository.save({
          ...payment,
          canceledAt: new Date(),
        });
    }
    // Save Cancellation
    return await this.repository.save({
      ...contract,
      cancelled_at: new Date(),
    });
  }

  public async refundPayment(contract: Contract, payment: Payment) {
    const refund = new Payment({
      contract,
      price: payment.price,
      refund: true,
    });
    this.paymentRepository.save(refund);
  }

  public async sign(id: number): Promise<Contract> {
    const contract = await this.repository.findOneBy({ id });

    const payment = await this.paymentRepository.findOne({
      where: { contract: { id } },
    });
    await this.paymentRepository.save({ ...payment, executedAt: new Date() });

    return await this.repository.save({
      ...contract,
      signed_at: new Date(),
    });
  }

  public async renewal(id: number): Promise<Contract> {
    const contract = await this.repository.findOneBy({ id });

    // cancel a refund if any on process
    const payment = await this.paymentRepository.findOne({
      where: { contract: { id }, refund: true },
    });
    if (payment)
      await this.paymentRepository.save({ ...payment, canceledAt: new Date() });

    return await this.repository.save({
      ...contract,
      cancelled_at: null,
      signed_at: new Date(),
    });
  }

  public addDaysToDate(date: Date, days: number): Date {
    const newDate = new Date(date);
    return new Date(newDate.setDate(newDate.getDate() + days));
  }

  public genUuidContract(): string {
    try {
      return uuidv4();
    } catch (e) {
      return "Cannot generate uuid , please contact support" ;
    }
  }

  public async create(userId: number, data: any): Promise<Contract> {
    const client = await this.clientRepository.findOne({
      relations: {
        user: true,
      },
      where: { user: { id: userId } },
    });
    const contract: Contract = new Contract({
      ...data,
      uuid: this.genUuidContract(),
      client: client,
      start_date: new Date(),
      end_date: new Date(new Date().setFullYear(new Date().getFullYear() + 1)),
    });
    return await this.repository.save(contract);
  }
}
