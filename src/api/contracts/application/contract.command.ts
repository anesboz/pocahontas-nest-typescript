import { IsBoolean, isDate, IsDate, IsEmail, IsString } from 'class-validator';

export class ContractCommand {
  @IsDate()
  start_date: Date;

  @IsDate()
  end_date: Date;

  @IsBoolean()
  auto_renewal: boolean;
}
