import { Controller, Get, UseGuards } from '@nestjs/common';
import { Role } from '../../users/domain/role/role.enum';
import { Roles } from '../../users/domain/role/roles.decorator';
import { AuthenticatedGuard } from '../../auth/guard/authenticated.guard';
import { ClientsService } from '../application/clients.service';
import { Client } from '../domain/client.entity';

@Controller('clients')
@UseGuards(AuthenticatedGuard)
@Roles(Role.Admin)
export class ClientController {
  constructor(private readonly service: ClientsService) {}
  @Get()
  async getClients(): Promise<Client[]> {
    return this.service.find();
  }
}
