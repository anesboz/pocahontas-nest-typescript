import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Client } from './domain/client.entity';
import { ClientsService } from './application/clients.service';
import { ClientController } from './interfaces/client.controller';
import { UsersService } from '../users/application/users.service';
import { User } from '../users/domain/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Client, User])],
  providers: [ClientsService, UsersService],
  controllers: [ClientController],
  exports: [ClientsService],
})
export class ClientsModule {}
