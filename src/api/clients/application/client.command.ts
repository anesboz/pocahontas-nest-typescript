import { IsEmail, IsString } from 'class-validator';

export class ClientCommand {
  @IsString()
  address: string;
}
