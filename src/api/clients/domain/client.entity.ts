import {
  Column,
  Entity,
  OneToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  JoinColumn,
} from 'typeorm';
import { Contract } from '../../contracts/domain/contract.entity';
import { User } from '../../users/domain/user.entity';
import { Quote } from '../../quote/domain/quote.entity';
import { Logement } from '../../quote/domain/logement/logement.entity';

@Entity()
export class Client {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ type: 'varchar', length: 255, nullable: true })
  address: string;

  @OneToOne(() => User)
  @JoinColumn()
  user: User;

  @OneToMany(() => Contract, (contract) => contract.client)
  contracts: Contract[];

  @OneToMany(() => Quote, (quote) => quote.client)
  quotes: Quote[];

  @OneToMany(() => Logement, (logement) => logement.client)
  logements: Logement[];
}
