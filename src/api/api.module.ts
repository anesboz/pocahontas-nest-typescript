import { Module } from '@nestjs/common';
import { QuoteModule } from './quote/quote.module';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { ContractsModule } from './contracts/contracts.module';
import { ClientsModule } from './clients/clients.module';
import { PaymentsModule } from './payments/payments.module';

@Module({
  imports: [
    QuoteModule,
    AuthModule,
    UsersModule,
    ContractsModule,
    ClientsModule,
    PaymentsModule,
  ],
})
export class ApiModule {}
