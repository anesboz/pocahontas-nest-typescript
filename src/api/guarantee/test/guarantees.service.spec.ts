import { Test, TestingModule } from '@nestjs/testing';
import { Repository } from 'typeorm';
import { createMock } from '@golevelup/ts-jest';
import { getRepositoryToken } from '@nestjs/typeorm';
import { GuaranteeService } from '../application/guarantee.service';
import { Guarantee } from '../domain/guarantee.entity';

describe('GuaranteeService', () => {
  let service: GuaranteeService;
  let repo: Repository<Guarantee>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GuaranteeService,
        {
          provide: getRepositoryToken(Guarantee),
          useValue: createMock<Repository<Guarantee>>(),
        },
      ],
    }).compile();

    service = module.get<GuaranteeService>(GuaranteeService);
    repo = module.get<Repository<Guarantee>>(getRepositoryToken(Guarantee));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
