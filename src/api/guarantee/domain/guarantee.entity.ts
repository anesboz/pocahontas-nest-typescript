import { Offer } from 'src/api/offer/domain/offer.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  Double,
  ManyToMany,
  JoinColumn,
} from 'typeorm';
import { GuaranteeCategory } from './categories/categories.enum';

@Entity()
export class Guarantee {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ type: 'varchar', length: 255, nullable: false, unique: true })
  name: string;

  @Column({ type: 'int', nullable: true, default: 0 })
  cost: number;

  @Column({ type: 'decimal', nullable: false, default: 100 })
  coverage: number; // Percentage from 0 to 100

  @Column({
    type: 'enum',
    enum: GuaranteeCategory,
    default: GuaranteeCategory.UNDEFINED,
  })
  category: GuaranteeCategory;

  // @ManyToMany(() => Offer)
  // @JoinColumn()
  // offers: Offer[];

  constructor(partial: Partial<Guarantee>) {
    Object.assign(this, partial);
  }
}
