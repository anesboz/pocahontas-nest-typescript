export enum GuaranteeCategory {
  UNDEFINED = 'non specifié',
  WALLS = 'murs',
  FLOOR = 'sol',
  FURNITURE = 'meubles',
  ELECTONICS = 'produits electroniques',
  WATER = 'dégat des eaux',
  FIRE = 'incendies',
  THEFT = 'cambriolage',
}
