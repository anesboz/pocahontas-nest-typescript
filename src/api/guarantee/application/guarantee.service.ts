import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Guarantee } from '../domain/guarantee.entity';
import { GuaranteeCommand } from './guarantee.command';

@Injectable()
export class GuaranteeService {
  constructor(
    @InjectRepository(Guarantee)
    private readonly repository: Repository<Guarantee>,
  ) {}

  async create(command: GuaranteeCommand): Promise<Guarantee> {
    if (await this.findOneByGuaranteeName(command.name)) {
      throw new HttpException(
        `Guarantee ${command.name} already exist`,
        HttpStatus.CONFLICT,
      );
    }
    const guarantee = new Guarantee(command);
    return this.repository.save(guarantee);
  }

  find(): Promise<Guarantee[]> {
    return this.repository.find();
  }

  delete(id: number) {
    return this.repository.delete({ id });
  }

  async findOneByGuaranteeName(
    name: string,
  ): Promise<Guarantee | undefined> | undefined {
    return this.repository.findOne({ where: { name } });
  }

  async findOneByid(id: number): Promise<Guarantee | undefined> | undefined {
    return this.repository.findOne({ where: { id } });
  }
}
