import {
  IsEnum,
  IsNumberString,
  IsOptional,
  IsString,
  Max,
  Min,
} from 'class-validator';
import { GuaranteeCategory } from '../domain/categories/categories.enum';

export class GuaranteeCommand {
  @IsString()
  name: string;

  @IsNumberString()
  cost: number;

  // @IsNumberString()
  // @IsOptional()
  // @Min(0)
  // @Max(100)
  coverage: number;

  @IsEnum(GuaranteeCategory)
  @IsOptional()
  category: GuaranteeCategory;
}
