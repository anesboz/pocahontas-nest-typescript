import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Guarantee } from './domain/guarantee.entity';
import { GuaranteeService } from './application/guarantee.service';
import { GuaranteeController } from 'src/front/guarantee/guarantee.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Guarantee])],
  providers: [GuaranteeService],
  controllers: [GuaranteeController],
  exports: [GuaranteeService],
})
export class GuaranteeModule {}
