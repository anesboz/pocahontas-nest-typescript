import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { QuoteUsecase } from './application/quote.usecase';
import { Quote } from './domain/quote.entity';
import { Logement } from './domain/logement/logement.entity';
import { Client } from '../clients/domain/client.entity';
import { AdresseProxy } from './infra/adresse.api/adresse.api.service';
import { HttpModule } from '@nestjs/axios';
import { UninsurableZoneService } from './application/uninsurable-zone.service';
import { UninsurableZoneEntity } from './domain/uninsurable-zone/uninsurable-zone.entity';

@Module({
  imports: [
    HttpModule,
    TypeOrmModule.forFeature([Logement, Quote, Client, UninsurableZoneEntity]),
  ],
  controllers: [],
  providers: [QuoteUsecase, AdresseProxy, UninsurableZoneService],
  exports: [QuoteUsecase, AdresseProxy, UninsurableZoneService],
})
export class QuoteModule {}
