import { IsString } from 'class-validator';

export class UninsurableZoneCommand {
  @IsString()
  public streetPattern: string;

  @IsString()
  public codePattern: string;

  @IsString()
  public cityPattern: string;
}
