import {
  IsBooleanString,
  IsEnum,
  IsNumberString,
  IsString,
} from 'class-validator';
import { OccupantType } from '../domain/occupant/occupant.type';
import { LogementType } from '../domain/logement/logement.type';
import { ResidenceType } from '../domain/logement/residence.type';

export class CreateQuoteCommand {
  @IsEnum(OccupantType)
  type: OccupantType;

  @IsBooleanString()
  isLivingOnResidence: boolean;

  @IsBooleanString()
  isStudent: boolean;

  @IsNumberString()
  livingPersons: number;

  @IsEnum(LogementType)
  logementType: LogementType;

  @IsEnum(ResidenceType)
  residenceType: ResidenceType;

  @IsNumberString()
  area: number;

  @IsNumberString()
  mainParts: number;

  @IsString()
  street: string;

  @IsNumberString()
  code: number;

  @IsString()
  city: string;
}
