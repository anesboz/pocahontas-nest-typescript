import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UninsurableZoneEntity } from '../domain/uninsurable-zone/uninsurable-zone.entity';
import { UninsurableZoneCommand } from './UninsurableZoneCommand';

@Injectable()
export class UninsurableZoneService {
  constructor(
    @InjectRepository(UninsurableZoneEntity)
    private readonly repository: Repository<UninsurableZoneEntity>,
  ) {}

  find(): Promise<UninsurableZoneEntity[]> {
    return this.repository.find();
  }

  add(command: UninsurableZoneCommand): Promise<UninsurableZoneEntity> {
    const zone = new UninsurableZoneEntity(
      command.streetPattern,
      command.codePattern,
      command.cityPattern,
    );
    return this.repository.save(zone);
  }
}
