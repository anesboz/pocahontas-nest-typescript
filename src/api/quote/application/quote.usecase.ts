import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateQuoteCommand } from './quote.command';
import { Quote } from '../domain/quote.entity';
import { toModel } from './quote.mapper';
import { Client } from '../../clients/domain/client.entity';
import { AdresseProxy } from '../infra/adresse.api/adresse.api.service';
import { UninsurableZoneEntity } from '../domain/uninsurable-zone/uninsurable-zone.entity';

@Injectable()
export class QuoteUsecase {
  constructor(
    @InjectRepository(Client)
    private readonly clientRepository: Repository<Client>,
    @InjectRepository(Quote)
    private readonly repository: Repository<Quote>,
    private readonly adresseProxy: AdresseProxy,
    @InjectRepository(UninsurableZoneEntity)
    private readonly uninsurableZoneRepository: Repository<UninsurableZoneEntity>,
  ) {}

  public async get(id: number): Promise<Quote> {
    return this.repository.findOne({
      where: { id },
      relations: { logement: true, occupant: true },
    });
  }

  public async update(quote: Quote): Promise<Quote> {
    return this.repository.save(quote);
  }

  public async findAll(): Promise<Quote[]> {
    return this.repository.find({
      relations: { logement: true, occupant: true },
      loadEagerRelations: true,
    });
  }

  public async create(
    command: CreateQuoteCommand,
    userId: number,
  ): Promise<Quote> {
    const client = await this.clientRepository.findOne({
      relations: {
        user: true,
      },
      where: { user: { id: userId } },
    });
    await this.checkAdresse(command);

    return this.repository.save(toModel(command, client));
  }

  private async checkAdresse({ street, code, city }) {
    if (!(await this.adresseProxy.exist({ street, code, city }))) {
      throw new HttpException(
        `L'adresse ${street} ${code}, ${city} invalide`,
        HttpStatus.NOT_FOUND,
      );
    }
    const uninsurableZones = await this.uninsurableZoneRepository.find();
    if (uninsurableZones.find((zone) => zone.isMatch({ street, code, city }))) {
      throw new HttpException(
        `l'adresse ${street} ${code}, ${city} non assurable`,
        HttpStatus.METHOD_NOT_ALLOWED,
      );
    }
  }

  deleteById(quoteId: number) {
    return this.repository.delete({
      id: quoteId,
    });
  }

  calcPrime(quote: Quote) {
    return 10 + (quote.logement.area % 5);
  }
}
