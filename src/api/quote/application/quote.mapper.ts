import { CreateQuoteCommand } from './quote.command';
import { Quote } from '../domain/quote.entity';
import { Occupant } from '../domain/occupant/occupant.entity';
import { Address } from '../domain/logement/address.entity';
import { Logement } from '../domain/logement/logement.entity';
import { Client } from '../../clients/domain/client.entity';

export const toModel = (command: CreateQuoteCommand, client: Client): Quote => {
  const occupant = new Occupant({ ...command });
  const address = new Address({ ...command });
  const logement = new Logement({ ...command, address, client });
  return new Quote({ logement, occupant, client });
};
