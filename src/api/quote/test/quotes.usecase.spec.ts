import { Test, TestingModule } from '@nestjs/testing';
import { Repository } from 'typeorm';
import { createMock } from '@golevelup/ts-jest';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ClientsService } from '../../clients/application/clients.service';
import { Client } from '../../clients/domain/client.entity';
import { Quote } from '../domain/quote.entity';
import { QuoteUsecase } from '../application/quote.usecase';
import { UninsurableZoneEntity } from '../domain/uninsurable-zone/uninsurable-zone.entity';
import { AdresseProxy } from '../infra/adresse.api/adresse.api.service';
import { HttpModule } from '@nestjs/axios';

describe('UsersService', () => {
  let underTest: QuoteUsecase;
  let repo: Repository<Quote>;
  let uninsurableZoneRepository: Repository<UninsurableZoneEntity>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        QuoteUsecase,
        AdresseProxy,
        {
          provide: getRepositoryToken(Quote),
          useValue: createMock<Repository<Quote>>(),
        },
        ClientsService,
        {
          provide: getRepositoryToken(Client),
          useValue: createMock<Repository<Client>>(),
        },
        {
          provide: getRepositoryToken(UninsurableZoneEntity),
          useValue: createMock<Repository<UninsurableZoneEntity>>(),
        },
      ],
      imports: [HttpModule],
    }).compile();

    underTest = module.get<QuoteUsecase>(QuoteUsecase);
    repo = module.get<Repository<Quote>>(getRepositoryToken(Quote));
    uninsurableZoneRepository = module.get<Repository<UninsurableZoneEntity>>(
      getRepositoryToken(UninsurableZoneEntity),
    );
  });

  it('should be defined', () => {
    expect(underTest).toBeDefined();
  });

  // it('get quote', async () => {
  //   // given
  //   const id = 1;
  //   const expected = new Quote({ id });
  //   repo.findOneBy = jest.fn().mockResolvedValue(expected);

  //   // when
  //   const quote = await underTest.get(id);

  //   // then
  //   expect(quote).toEqual(expected);
  //   expect(repo.findOneBy).toBeCalledTimes(1);
  //   expect(repo.findOneBy).toBeCalledWith({ id });
  // });

  // it('Create new quote', async () => {
  //   // given
  //   const command = new CreateQuoteCommand();
  //   command.area = 100;
  //   command.city = 'Paris';
  //   command.isStudent = false;
  //   const expected = toModel(command);
  //   repo.save = jest.fn().mockResolvedValue(expected);

  //   //when
  //   const quote = await underTest.create(command);

  //   // then
  //   expect(quote).toEqual(expected);
  //   expect(repo.save).toBeCalledWith(toModel(command));
  //   expect(repo.save).toBeCalledTimes(1);
  // });

  it('Delete quote', async () => {
    // given
    repo.delete = jest.fn();

    // when
    await underTest.deleteById(1);

    // then
    expect(repo.delete).toBeCalledTimes(1);
    expect(repo.delete).toBeCalledWith({ id: 1 });
  });

  // it('FindAll quote', async () => {
  //   // given
  //   const expected = [
  //     new Quote({ id: 1 }),
  //     new Quote({ id: 2 }),
  //     new Quote({ id: 3 }),
  //   ];
  //   repo.find = jest.fn().mockResolvedValue(expected);

  //   // when
  //   const quotes = await underTest.findAll();

  //   // then
  //   expect(quotes).toEqual(expected);
  //   expect(repo.find).toBeCalledTimes(1);
  //   expect(repo.find).toBeCalledWith({
  //     relations: { logement: true, occupant: true },
  //     loadEagerRelations: true,
  //   });
  // });
});
