import { Column } from 'typeorm';

import { OccupantType } from './occupant.type';
export class Occupant {
  @Column({ type: 'varchar', length: 20 })
  type: OccupantType;

  @Column({ type: 'boolean', default: false })
  isLivingOnResidence: boolean;

  @Column({ type: 'boolean', default: false })
  isStudent: boolean;

  @Column({ type: 'int', nullable: false, default: 0 })
  livingPersons: number;

  constructor(props: Partial<Occupant>) {
    Object.assign(this, props);
  }
}
