export enum OccupantType {
  LOCATAIRE = 'locataire',
  COLOCATAIRE = 'colocataire',
  PROPRIETAIRE = 'proprietaire',
}
