import { UninsurableZoneEntity } from './uninsurable-zone.entity';

describe('uninsurable-zone test', () => {
  it('7500 uninsurable ', function () {
    const zone = new UninsurableZoneEntity('\\*', '7500', '\\*');
    zone.codePattern = '7500';

    expect(
      zone.isMatch({
        street: '*',
        city: '*',
        code: '7500',
      }),
    ).toBeTruthy();

    expect(
      zone.isMatch({
        street: 'any',
        city: 'any',
        code: '9999',
      }),
    ).toBeFalsy();
  });

  it('75* uninsurable ', function () {
    const zone = new UninsurableZoneEntity('\\*', '75*', '\\*');

    expect(
      zone.isMatch({
        street: '*',
        city: '*',
        code: '7500',
      }),
    ).toBeTruthy();

    expect(
      zone.isMatch({
        street: '*',
        city: '*',
        code: '4500',
      }),
    ).toBeFalsy();
  });
});
