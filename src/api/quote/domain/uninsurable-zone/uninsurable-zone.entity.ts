import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { HttpException, HttpStatus } from '@nestjs/common';

@Entity()
export class UninsurableZoneEntity {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column({ nullable: true })
  public streetPattern: string;

  @Column({ nullable: true })
  public codePattern: string;

  @Column({ nullable: true })
  public cityPattern: string;

  constructor(streetPattern, codePattern, cityPattern) {
    // validate pattern
    try {
      new RegExp(streetPattern);
      new RegExp(codePattern);
      new RegExp(cityPattern);
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }

    //
    this.codePattern = codePattern;
    this.cityPattern = cityPattern;
    this.streetPattern = streetPattern;
  }

  isMatch({ street, code, city }) {
    return (
      new RegExp(this.streetPattern || '\\**').test(street) &&
      new RegExp(this.codePattern || '\\**').test(code) &&
      new RegExp(this.cityPattern || '\\**').test(city)
    );
  }
}
