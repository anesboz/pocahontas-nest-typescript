import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Occupant } from './occupant/occupant.entity';
import { Logement } from './logement/logement.entity';
import { Client } from '../../clients/domain/client.entity';
import { Contract } from '../../contracts/domain/contract.entity';

@Entity()
export class Quote {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column(() => Occupant)
  occupant: Occupant;

  @OneToOne(() => Logement, {
    cascade: true,
  })
  @JoinColumn({ foreignKeyConstraintName: 'logement_quote_fk' })
  public logement: Logement;

  @Column({ nullable: true })
  prime!: number;

  @ManyToOne(() => Client, (client) => client.quotes)
  @JoinColumn()
  client: Client;

  @OneToOne(() => Contract)
  contract: Contract;

  /*
   * Create and Update Date Columns
   */
  @CreateDateColumn({ type: 'timestamp' })
  public createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  public updatedAt!: Date;

  constructor(props: Partial<Quote>) {
    Object.assign(this, props);
  }
}
