import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { LogementType } from './logement.type';
import { ResidenceType } from './residence.type';
import { Address } from './address.entity';
import { Client } from '../../../clients/domain/client.entity';

@Entity()
export class Logement {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ type: 'varchar', length: 20 })
  logementType: LogementType;

  @Column({ type: 'varchar', length: 20 })
  residenceType: ResidenceType;

  @Column({ type: 'float' })
  area: number;

  @Column({ type: 'boolean', default: false })
  hasDependencies: boolean;

  @Column({ type: 'int', nullable: false, default: 0 })
  mainParts: number;

  @Column(() => Address)
  address: Address;

  @ManyToOne(() => Client, (client) => client.logements)
  @JoinColumn()
  client: Client;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt!: Date;

  constructor(props: Partial<Logement>) {
    Object.assign(this, props);
  }
}
