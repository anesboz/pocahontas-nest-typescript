import { Column } from 'typeorm';

export class Address {
  @Column({ type: 'varchar' })
  street: string;

  @Column({ type: 'varchar' })
  city: string;

  @Column({ type: 'integer' })
  code: number;

  constructor(props: Required<Address>) {
    Object.assign(this, props);
  }

  toString(): string {
    return `${this.street}, ${this.code} ${this.city}`;
  }
}
