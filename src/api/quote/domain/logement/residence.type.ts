export enum ResidenceType {
  MAIN = 'main',
  SECOND = 'second',
}
