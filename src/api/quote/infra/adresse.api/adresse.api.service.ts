import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { AdresseProperty, ApiAdresseResponse } from './model/adresse.api.model';

@Injectable()
export class AdresseProxy {
  private readonly baseUrl = 'https://api-adresse.data.gouv.fr';

  constructor(private readonly httpService: HttpService) {}

  private async search(pattern: string): Promise<AdresseProperty[]> {
    const response = await this.httpService.axiosRef.get<ApiAdresseResponse>(
      `${this.baseUrl}/search/?q=${pattern}`,
    );
    return response.data.features.flatMap((feature) => feature.properties);
  }

  async exist({ street, code, city }) {
    const properties = await this.search(`${street}+${code}+${city}`);
    return properties.find((property) => {
      return (
        property.name.toLowerCase() === street.toLowerCase() &&
        property.postcode === code &&
        property.city.toLowerCase() === city.toLowerCase()
      );
    });
  }
}
