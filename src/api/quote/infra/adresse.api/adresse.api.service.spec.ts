import { HttpModule } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { AdresseProxy } from './adresse.api.service';

describe('AdresseApiService', () => {
  let service: AdresseProxy;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AdresseProxy],
      imports: [HttpModule],
    }).compile();

    service = module.get<AdresseProxy>(AdresseProxy);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
