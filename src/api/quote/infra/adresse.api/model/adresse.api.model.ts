export interface ApiAdresseResponse {
  features: Feature[];
}

export interface Feature {
  properties: AdresseProperty[];
}

export interface AdresseProperty {
  label: string;
  name: string;
  postcode: string;
  city: string;
}
