import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './domain/user.entity';
import { UsersService } from './application/users.service';
import { RolesGuard } from './domain/role/roles.guard';
import { APP_GUARD } from '@nestjs/core';
import { UserController } from './interfaces/user.controller';
import { Client } from '../clients/domain/client.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Client])],
  providers: [
    UsersService,
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    },
  ],
  controllers: [UserController],
  exports: [UsersService],
})
export class UsersModule {}
