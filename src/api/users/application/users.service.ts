import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../domain/user.entity';
import { Repository } from 'typeorm';
import { UserCommand } from './user.command';
import { Client } from '../../clients/domain/client.entity';
import { Role } from '../domain/role/role.enum';
import { adminInfo } from '../domain/admin.info';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly repository: Repository<User>,
    @InjectRepository(Client)
    private readonly clientRepository: Repository<Client>,
  ) {}

  async findOneByUsername(
    username: string,
  ): Promise<User | undefined> | undefined {
    return this.repository.findOne({ where: { username } });
  }

  async findOneByid(id: number): Promise<User | undefined> | undefined {
    return this.repository.findOne({ where: { id } });
  }

  async save(command: UserCommand): Promise<User> {
    if (await this.findOneByUsername(command.username)) {
      throw new HttpException(
        `username ${command.username} already exist`,
        HttpStatus.CONFLICT,
      );
    }
    const user = new User(command);
    const client = new Client();
    client.user = user;
    return this.repository.save(user).then((res) => {
      this.clientRepository.save(client);
      return res;
    });
  }

  async find(): Promise<User[]> {
    return this.repository.find();
  }

  async createAdmin() {
    if (!(await this.findOneByUsername('admin'))) {
      await this.repository.save(new User(adminInfo));
    }
  }
}
