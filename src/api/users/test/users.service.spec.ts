import { Test, TestingModule } from '@nestjs/testing';
import { Repository } from 'typeorm';
import { createMock } from '@golevelup/ts-jest';
import { User } from '../domain/user.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UsersService } from '../application/users.service';
import { ClientsService } from '../../clients/application/clients.service';
import { Client } from '../../clients/domain/client.entity';
import { UserCommand } from '../application/user.command';
import { HttpException } from '@nestjs/common';

describe('UsersService', () => {
  let service: UsersService;
  let repo: Repository<User>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: createMock<Repository<User>>(),
        },
        ClientsService,
        {
          provide: getRepositoryToken(Client),
          useValue: createMock<Repository<Client>>(),
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
    repo = module.get<Repository<User>>(getRepositoryToken(User));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('Create new user', async () => {
    const command = new UserCommand();
    command.username = 'username';
    const excepted = new User(command);

    repo.findOne = jest.fn().mockResolvedValue(undefined);
    repo.save = jest.fn().mockResolvedValue(excepted);

    await expect(service.save(command)).resolves.toEqual(excepted);
  });

  it('Create existing user should throw', async () => {
    const command = new UserCommand();
    command.username = 'username';
    const existing = new User(command);

    repo.findOne = jest.fn().mockResolvedValue(existing);
    repo.save = jest.fn().mockResolvedValue(existing);

    await expect(service.save(command)).rejects.toThrow(HttpException);
  });
});
