import { RolesGuard } from '../domain/role/roles.guard';
import { Test, TestingModule } from '@nestjs/testing';

describe('RolesGuard', () => {
  let underTest: RolesGuard;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RolesGuard],
    }).compile();

    underTest = module.get<RolesGuard>(RolesGuard);
  });

  it('should be defined', () => {
    expect(underTest).toBeDefined();
  });
});
