import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Exclude } from 'class-transformer';
import { Role } from './role/role.enum';
import { Client } from '../../clients/domain/client.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ type: 'varchar', length: 120 })
  fullName;

  @Column({ type: 'varchar', unique: true, length: 120 })
  username;

  @Column({ type: 'varchar', length: 120 })
  email: string;

  @Exclude()
  @Column({ type: 'varchar', length: 120 })
  password;

  @OneToOne(() => Client, (client) => client.user)
  @Column({ type: 'varchar', length: 120 })
  roles: Role[] = [Role.User];

  constructor(partial: Partial<User>) {
    Object.assign(this, partial);
  }
}
