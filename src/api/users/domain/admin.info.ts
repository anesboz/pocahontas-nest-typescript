import { Role } from './role/role.enum';

export const adminInfo = {
  username: 'admin',
  password: 'admin',
  fullName: 'admin',
  email: 'admin@admin.com',
  roles: [Role.Admin],
};
