import { Controller, Get, UseGuards } from '@nestjs/common';
import { Role } from '../domain/role/role.enum';
import { Roles } from '../domain/role/roles.decorator';
import { AuthenticatedGuard } from '../../auth/guard/authenticated.guard';
import { UsersService } from '../application/users.service';
import { User } from '../domain/user.entity';

@Controller('users')
@UseGuards(AuthenticatedGuard)
@Roles(Role.Admin)
export class UserController {
  constructor(private readonly service: UsersService) {}
  @Get()
  async getUsers(): Promise<User[]> {
    return this.service.find();
  }
}
