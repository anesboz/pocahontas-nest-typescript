import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { QuoteController } from './front/quote/quote.controller';
import { getEnvPath } from './common/helper/env.helper';
import { TypeOrmConfigService } from './shared/typeorm/typeorm.service';
import { ApiModule } from './api/api.module';
import { QuoteModule } from './api/quote/quote.module';
import { AuthController } from './front/auth/auth.controller';
import { AuthModule } from './api/auth/auth.module';
import { AlreadyAuthMiddleware } from './api/auth/guard/already-auth.middleware';
import { AppController } from './front/app.controller';
import { ContractsModule } from './api/contracts/contracts.module';
import { ContractsController } from './front/contracts/contracts.controller';
import { UsersModule } from './api/users/users.module';
import { CreateQuoteController } from './front/quote/create-quote/create-quote.controller';
import { ClientsModule } from './api/clients/clients.module';
import { OfferModule } from './api/offer/offer.module';
import { OfferController } from './front/offer/offer.controller';
import { GuaranteeController } from './front/guarantee/guarantee.controller';
import { HttpModule } from '@nestjs/axios';
import { PaymentController } from './front/payments/payment.controller';
import { PaymentsModule } from './api/payments/payments.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { AdminController } from './front/admin/admin.controller';
import { DashboardController } from './front/dashboard/dashboard.controller';

const envFilePath: string = getEnvPath(`${__dirname}/common/envs`);

@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath, isGlobal: true }),
    TypeOrmModule.forRootAsync({ useClass: TypeOrmConfigService }),
    ApiModule,
    QuoteModule,
    AuthModule,
    ContractsModule,
    UsersModule,
    ClientsModule,
    OfferModule,
    HttpModule,
    PaymentsModule,
    // To send mails
    MailerModule.forRoot({
      transport: {
        host: 'smtp.sendgrid.net',
        port: 465,
        secure: true,
        auth: {
          user: 'apikey',
          pass: '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',
        },
      },
      defaults: {
        from: '"nest-modules" <modules@nestjs.com>',
      },
    }),
  ],
  controllers: [
    AdminController,
    QuoteController,
    AuthController,
    GuaranteeController,
    AppController,
    ContractsController,
    CreateQuoteController,
    OfferController,
    PaymentController,
    DashboardController,
  ],
  providers: [],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(AlreadyAuthMiddleware).forRoutes('/auth');
  }
}
